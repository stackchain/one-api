const express = require("express");
const cors = require("cors");
const helmet = require("helmet");

const app = express();
app.use(cors({
  origin: true
}));
app.use(helmet());
app.use(helmet.noCache());
app.options("*", cors());


app.get("/:status", function(req, res, next) {
  console.log("<= ", req.params);
  let status = 200;
  if (!isNaN(req.params.status) && req.params.status < 600  && req.params.status > 200) {
    status = req.params.status; 
  }
  console.log("=> ", status);
  res.status(status || 200).json({});
});


app.listen(3003, function(req, res, next) {
  console.log(req);
});