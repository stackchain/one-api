const express = require("express");
const cors = require("cors");
const helmet = require("helmet");
const https = require("https");
const http = require("http");

const users = require("./v1/routes/users");
const customers = require("./tokens");

import { ERROR_NOT_AUTHORIZED, ERROR_FORBIDDEN } from "./v1/contants";

/************ SERVER CONFIGS ************/
const app = express();
app.use(cors({
  origin: true
}));
app.use(helmet());
app.use(helmet.noCache());
app.options("*", cors());

/** ALL VERSION UNDER MAINTENANCE WILL BE KEPT HERE **/
/************ ROUTES V-LATEST ************/
const v1 = express.Router();
var customer = {};

// Can you request me?
v1.use((req, res, next) => {
  const list = Object.keys(customers);
  const foundIt = list.filter(d => d === req.get("origin"));
  if (foundIt.length === 0) {
    return res.status(401).json(ERROR_NOT_AUTHORIZED);
  } else {
    customer = customers[foundIt[0]];
    return next();
  }
});

// Ask for permission to edit the context
function isAllowed (req, res, next) {
  if (customer.byPass || req.path.match(/mock/)) return next();
  if (!customer.byPass) {
    const mod = customer.https ? https : http;
    const oneHeaders = {
      "X-one-api-resource": req.originalUrl,
      "X-one-api-data": req.params,
      "X-one-api-method": req.method
    };
    customer.api["headers"] = { ...customer.api["headers"], ...oneHeaders };
    try {
      const authReq = mod.request(customer.api, (authRes) => {
        if (authRes.statusCode !== 200) {
          return res.status(403).json(ERROR_FORBIDDEN);
        }
        authRes.on("data", () => {
        });
        authRes.on("end", () => {
          return next();
        });
      });
      authReq.on("error", (err) => {
        return res.status(403).json(err);
      });
      authReq.end();
    } catch (err) {
      return res.status(500).json(err);
    }
  }
}

users.translator.get.bind(customer);

/**************** USERS *****/
v1.use("/users", express.Router()
  .get("/:id", isAllowed, (req, res) => users.translator.get(req, res, customer))
  .put("/:id", isAllowed, (req, res) => users.translator.put(req, res, customer)));

v1.use("/test", express.Router()
  .get("/", (req, res) => {
    res.json();
  }));


app.use("/v1", v1);
app.use("/", v1);
/****************************************/

module.exports = app;



// app.use("/api-doc", express.Router()
//   .get("", (req, resp, next) => {
//     resp.send(swagger.apidoc());
//     next();
//   }));

// TODO: Rewrite the entire openapi source code
// const Swagger = require("openapi-doc");
// const swagger = new Swagger();

// API doc - //TODO: to document the operations we will pass the swagger instance down
// swagger.info("OnePercent.IO", "v1", "This is the API that handles all OnePercent.IO tools/widgets/etc...");
// swagger.contact("Juliano Lazzarotto aka stackchain", "lets.talk@onepercent.io");
// swagger.termsOfService("To use this API you must agree with the terms of use that is hosted on https://onepercent.io/api/terms-of-use.html, if you are consuming this API it meant that you already agreed with that.");
// swagger.license("MIT");

