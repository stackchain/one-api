const tokens = {
  "https://127.0.0.1:8080": {
    byPass: true,
    context: "dev",
    https: false,
    api: {
      hostname: "127.0.0.1",
      port: 3003,
      path: "/200",
      method: "GET",
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Content-Type": "application/json; charset=utf-8"
      }
    }
  },
  "http://127.0.0.1:8080": {
    byPass: true,
    context: "dev",
    https: false,
    api: {
      hostname: "127.0.0.1",
      port: 3003,
      path: "/200",
      method: "GET",
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Content-Type": "application/json; charset=utf-8"
      }
    }
  },
  "https://assets.onepercent.io": {
    byPass: true,
    context: "one",
  },
  "https://staging-assets.onepercent.io": {
    byPass: true,
    context: "dev"
  }
};

module.exports = tokens;
