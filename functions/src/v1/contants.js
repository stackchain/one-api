export const EMPTY_WALLET = {
  wallets: {
    ETH: []
  }
};

export const ERROR_NOT_AUTHORIZED = {
  Error: "Please subscribe for an authorization."
};

export const ERROR_FORBIDDEN = {
  Error: "Operation not allowed, check your backend log."
};