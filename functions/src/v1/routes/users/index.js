const functions = require("firebase-functions");
const admin = require("firebase-admin");
const Joi = require("joi");
const moment = require("moment");

import { EMPTY_WALLET as wallets } from "../../contants";

export const translator = {

  async get(req, res, customer) {
    try {
      const id = req.params.id;
      const context = customer.context;
      const result = await this.adapter.get(id, context);
      return res.json(result);
    } catch (err) {
      return res.status(400).json(err);
    }
  },

  async validatePutData(data) {
    const schema = Joi.object({
      wallets: Joi.object().keys({
        ETH: Joi.array().required()
      }),
      updated: Joi.date().default(moment().toISOString()).iso().forbidden(),
    });

    const { error, value } = Joi.validate(data, schema);
    if(!error) return value;

    let messages = error.details.map(e => e.message);

    throw new Error(messages.toString());
  },

  async put(req, res, customer) {
    try {
      const id = req.params.id;
      const data = req.body;
      const context = customer.context;
      const body = await translator.validatePutData(data);
      const result = await this.adapter.save(id, body, context);
      return res.status(200).json(result);
    } catch (err) {
      return res.status(400).json(err);
    }
  },

  adapter: {

    async get(id, context) {
      const result = await admin.firestore().collection(`/${context}-users`).doc(id).get();
      return result.exists ? {
        id: result.id,
        ...result.data()
      } : {
        id: id,
        ...wallets
      };
    },
  
    async save(id, data, context) {
      const result = await admin.firestore().collection(`/${context}-users`).doc(id).set(data, { merge: true });
      return { id: result.id, ...data };
    }
  }

};